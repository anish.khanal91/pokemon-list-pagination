import axios from 'axios';
import React, {useState, useEffect} from 'react';
import Pagination from './Pagination';
import PokemonList from './PokemonList';

function App() {
  const [pokemon,setPokemon] = useState([])
  const [currentPageUrl,setCurrentPageUrl] = useState("https://pokeapi.co/api/v2/pokemon")
  const [nextPageUrl,setNextPageUrl] = useState()
  const [loading,setLoading] = useState(true)
  const [prevPageUrl,setPrevPageUrl] = useState()
  useEffect(() => {
    setLoading(true)
    axios.get(currentPageUrl).then(res => {
      setLoading(false)
      setNextPageUrl(res.data.next)
      setPrevPageUrl(res.data.previous)
      setPokemon(res.data.results.map(p => p.name))
    })
  }, [currentPageUrl])

  function goToNextPage() {
    setCurrentPageUrl(nextPageUrl)
  }
  function goToPrevPage() {
    setCurrentPageUrl(prevPageUrl)
  }
  if(loading) return "Loading..."

  return (
   <><PokemonList pokemon={pokemon} />
   <Pagination 
   goToNextPage={nextPageUrl ? goToNextPage : null}
   goToPrevPage={prevPageUrl ? goToPrevPage : null}
   />
   </>
  );
}

export default App